%global debug_package %{nil}

# Global variables for github repository
%global commit0 20cc5244d08de13668a36749b7b165507b4805bf
%global gittag0 v2.0.0
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name:           Carla
Version:        2.0.0
Release:        1%{?dist}
Summary:        A rack manager JACK

Group:          Applications/Multimedia
License:        GPLv2+
URL:            https://github.com/falkTX/Carla
Source0:        https://github.com/falkTX/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires: python-qt5-devel
BuildRequires: python-magic
BuildRequires: liblo-devel
BuildRequires: alsa-lib-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: gtk2-devel
BuildRequires: gtk3-devel
BuildRequires: qt-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: fluidsynth-devel
BuildRequires: fftw-devel
BuildRequires: mxml-devel
BuildRequires: zlib-devel
BuildRequires: mesa-libGL-devel
BuildRequires: non-ntk-fluid
BuildRequires: non-ntk-devel
BuildRequires: jack-audio-connection-kit-devel
#BuildRequires: linuxsampler-devel
#BuildRequires: libprojectM-devel

Requires(pre): python3-qt5

%package -n Carla-lv2
Summary:       LV2 version of the Carla plugin host
Group:         Applications/Multimedia
Requires:      lv2
Requires:      Carla

%description -n Carla-lv2
LV2 version of the Carla plugin host

%package -n Carla-vst
Summary:       LV2 version of the Carla plugin host
Group:         Applications/Multimedia
Requires:      Carla

%description -n vst-Carla
LV2 version of the Carla plugin host

%description
A rack manager for JACK

%prep
%setup -qn %{name}-%{commit0}

%build
make DESTDIR=%{buildroot} PREFIX=/usr LIBDIR=%{_libdir} %{?_smp_mflags}

%install 
make DESTDIR=%{buildroot} PREFIX=/usr LIBDIR=%{_libdir}  %{?_smp_mflags} install

# Create a vst directory
%__install -m 755 -d %{buildroot}/%{_libdir}/vst/

%post 
update-desktop-database -q
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
update-desktop-database -q
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans 
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%{_bindir}/*
%{_includedir}/carla/*
%{_libdir}/carla/*
%{_libdir}/pkgconfig/*
#%{_libdir}/python3/*
%{_datadir}/applications/*
%{_datadir}/carla/*
%{_datadir}/icons/*
%{_datadir}/mime/*

%files -n Carla-lv2
%{_libdir}/lv2/*

%files -n Carla-lv2
%{_libdir}/vst/

%changelog
* Fri Jul 5 2019 Drew DeVore <drew@devorcula.com> - 2.0.0
- Updated to latest release and split out plugin versions
* Tue Sep 18 2018 Drew DeVore <drew@devorcula.com> - 2.0-RC2
- Updated to latest release candidate
* Sun Aug 05 2018 Drew DeVore <drew@devorcula.com> - 2.0.0beta-7
- Updated to latest release candidate
* Thu Jul 05 2018 Drew DeVore <drew@devorcula.com> - 2.0.0beta-6-2
- Updated to latest release candidate
* Sat May 12 2018 Yann Collette <ycollette.nospam@free.fr> - 2.0.0beta-5
- update to latest master
* Tue May 1 2018 Yann Collette <ycollette.nospam@free.fr> - 2.0.0beta-4
- version 4
- update default folders
- update to master
* Wed Nov 22 2017 Yann Collette <ycollette.nospam@free.fr> - 2.0.0beta
- add a missing requires
* Mon Oct 23 2017 Yann Collette <ycollette.nospam@free.fr> - 2.0.0beta
- update to latest master
* Sat Jun 06 2015 Yann Collette <ycollette.nospam@free.fr> - 2.0.0beta
- Initial build
