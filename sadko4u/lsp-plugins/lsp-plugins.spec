%global debug_package %{nil}

Name:           lsp-plugins
Version:        1.1.13
Release:        1%{?dist}
ExclusiveArch:  x86_64
Summary:        A collection of open-source plugins to fill the lack of good and useful plugins under the GNU/Linux platform.

Group:          Applications/Multimedia
License:        Custom
URL:            http://lsp-plug.in
Source0:        https://svwh.dl.sourceforge.net/sourceforge/lsp-plugins/lsp-plugins-lv2-%{version}-Linux-x86_64.tar.gz
Source1:        https://iweb.dl.sourceforge.net/sourceforge/lsp-plugins/lsp-plugins-lxvst-%{version}-Linux-x86_64.tar.gz
Requires:       glibc
Requires:       libsndfile
Requires:       cairo
Requires:       jack-audio-connection-kit or jack-audio-connection-kit-dbus
Requires:       lv2

%description
A collection of open-source plugins to fill the lack of good and useful plugins under the GNU/Linux platform.

%package common
Summary:        A collection of open-source plugins to fill the lack of good and useful plugins under the GNU/Linux platform.
Group:          Applications/Multimedia

%description common
A collection of open-source plugins to fill the lack of good and useful plugins under the GNU/Linux platform.

%package -n lv2-%{name}
Summary:        A collection of open-source LV2 plugins to fill the lack of good and useful plugins under the GNU/Linux platform.
Group:          Applications/Multimedia
Requires:       lsp-plugins-common

%description -n lv2-%{name}
A collection of LV2 open-source plugins to fill the lack of good and useful plugins under the GNU/Linux platform.

%package -n vst-%{name}
Summary:        A collection of open-source VST plugins to fill the lack of good and useful plugins under the GNU/Linux platform.
Group:          Applications/Multimedia
Requires:       lsp-plugins-common

%description -n vst-%{name}
A collection of VST open-source plugins to fill the lack of good and useful plugins under the GNU/Linux platform.

%prep
%autosetup -n lsp-plugins-lv2-%{version}-Linux-x86_64
%autosetup -n lsp-plugins-lv2-%{version}-Linux-x86_64 -a 1

%build

%install
mkdir -p %{buildroot}%{_datadir}/doc/lsp-plugins
mkdir -p %{buildroot}%{_libdir}/lv2/lsp-plugins.lv2
mkdir -p %{buildroot}%{_libdir}/vst/lsp-plugins.vst
install -p -m 744 *.txt %{buildroot}%{_datadir}/doc/lsp-plugins/
install -p -m 744 lsp-plugins.lv2/* %{buildroot}%{_libdir}/lv2/lsp-plugins.lv2/
install -p -m 744 lsp-plugins-lxvst-%{version}-Linux-x86_64/*.so %{buildroot}%{_libdir}/vst/lsp-plugins.vst/

%clean

%files common
%doc CHANGELOG.txt LICENSE.txt README.txt
%{_datadir}/doc/lsp-plugins/

%files -n lv2-%{name}
%{_libdir}/lv2/lsp-plugins.lv2/*

%files -n vst-%{name}
%{_libdir}/vst/lsp-plugins.vst/*.so

%changelog
* Fri Feb 28 2020 Drew DeVore <drew@devorcula.com> - 1.1.13
- Updated to 1.1.13

* Wed Jul 24 2019 Drew DeVore <drew@devorcula.com> - 1.1.10-1
- Combined LV2 and VST and updated to 1.1.10

* Fri Jul 5 2019 Drew DeVore <drew@devorcula.com> - 1.1.9-1
- Updated to the latest version

* Tue Aug 14 2018 Drew DeVore <drew@devorcula.com> - 1.1.3-1
- Initial build
